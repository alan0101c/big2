package big2;

import card.Deck;

public class BigTwoDeck extends Deck {

    /**
     * Initialize the deck with 52 BigTwo cards
     */
    public void initialize() {
        removeAllCards();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 13; j++) {
                BigTwoCard btcard = new BigTwoCard(i, j);
                addCard(btcard);
            }
        }
    }

}
