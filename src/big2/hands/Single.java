package big2.hands;

import card.CardGamePlayer;
import card.CardList;
import card.Hand;

public class Single extends Hand {

    public Single(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    public boolean isValid() {
        if (this.size() == 1)
            return true;
        else
            return false;
    }

    public String getType() {
        return "The Single";
    }

    public boolean beats(Hand hand) {
        if (hand.getType() == "The Single") {
            return this.getTopCard().compareTo(hand.getTopCard()) > 0;
        } else {
            return false;
        }
    }
}
