package big2.hands;

import card.Card;
import card.CardGamePlayer;
import card.CardList;
import card.Hand;

public class Quad extends Hand {
    public Quad(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    public String getType() {
        return "Quad";
    }

    public boolean isValid() {
        sort();
        if (this.size() == 5 && this.getCard(0).getRank() == this.getCard(1).getRank() && this.getCard(1).getRank() == this.getCard(2).getRank() && this.getCard(2).getRank() == this.getCard(3).getRank()) {

            return true;
        } else if (this.size() == 5 && this.getCard(1).getRank() == this.getCard(2).getRank() && this.getCard(2).getRank() == this.getCard(3).getRank() && this.getCard(3).getRank() == this.getCard(4).getRank()) {
            return true;
        } else {
            return false;
        }

    }

    public Card getTopCard() {
        sort();
        if (this.getCard(0).getRank() == this.getCard(1).getRank() && this.getCard(1).getRank() == this.getCard(2).getRank() && this.getCard(2).getRank() == this.getCard(3).getRank()) {
            return (this.getCard(0));
        } else {
            return this.getCard(4);
        }
    }


    public boolean beats(Hand hand) {
        if (hand.getType() == "StraightFlush") {
            return false;
        } else if (hand.getType() == "Quad")
            if (this.getTopCard().compareTo(hand.getTopCard()) == 1) {
                return true;
            } else {
                return false;
            }
        else {
            return true;
        }


    }
}
