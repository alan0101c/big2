package big2.hands;

import card.CardGamePlayer;
import card.CardList;
import card.Hand;

public class Triple extends Hand {
    public Triple(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    public String getType() {
        return "Triple";
    }

    public boolean isValid() {
        if (this.size() == 3 && this.getCard(0).getRank() == this.getCard(1).getRank()) {
            if (this.getCard(1).getRank() == this.getCard(2).getRank()) {

                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    public boolean beats(Hand hand) {
        if (hand.getType() == "Triple") {
            return this.getTopCard().compareTo(hand.getTopCard()) > 0;
        } else {
            return false;
        }
    }
}
