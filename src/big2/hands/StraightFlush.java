package big2.hands;

import card.CardGamePlayer;
import card.CardList;
import card.Hand;

public class StraightFlush extends Hand {
    public StraightFlush(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    public String getType() {
        return "StraightFlush";
    }

    public boolean isValid() {
        sort();
        if (this.size() == 5 && this.getCard(0).getSuit() == this.getCard(1).getSuit() && this.getCard(1).getSuit() == this.getCard(2).getSuit() && this.getCard(2).getSuit() == this.getCard(3).getSuit() && this.getCard(3).getSuit() == this.getCard(4).getSuit()) {

            if (this.size() == 5 && this.getCard(0).getRank() > 1 && this.getCard(0).getRank() < 11) {
                if (this.getCard(0).getRank() < 9 && (this.getCard(1).getRank() - this.getCard(0).getRank() == 1) && (this.getCard(2).getRank() - this.getCard(1).getRank() == 1) && (this.getCard(3).getRank() - this.getCard(2).getRank() == 1) && (this.getCard(4).getRank() - this.getCard(3).getRank() == 1)) {

                    return true;
                } else if (this.getCard(0).getRank() == 9 && this.getCard(1).getRank() == 10 && this.getCard(1).getRank() == 11 && this.getCard(1).getRank() == 12 && this.getCard(1).getRank() == 0) {
                    return true;
                } else if (this.getCard(0).getRank() == 10 && this.getCard(1).getRank() == 11 && this.getCard(1).getRank() == 12 && this.getCard(1).getRank() == 0 && this.getCard(1).getRank() == 1) {

                    return true;
                }
            } else {
                return false;
            }
            return false;
        }
        return false;
    }

    public boolean beats(Hand hand) {
        if (hand.getType() == "StraightFlush") {
            if (this.getCard(0).getSuit() == hand.getCard(0).getSuit()) {
                if (this.getTopCard().compareTo(hand.getTopCard()) == 1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
