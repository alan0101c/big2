package big2.hands;

import card.CardGamePlayer;
import card.CardList;
import card.Hand;

public class Double extends Hand {
    public Double(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    public String getType() {
        return "Double";
    }

    public boolean isValid() {
        if (this.size() == 2 && this.getCard(0).getRank() == this.getCard(1).getRank()) {

            return true;
        } else {
            return false;
        }
    }

    public boolean beats(Hand hand) {
        if (hand.getType() == "Double") {
            return this.getTopCard().compareTo(hand.getTopCard()) > 0;
        } else {
            return false;
        }
    }
}
