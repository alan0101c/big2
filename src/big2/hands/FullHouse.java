package big2.hands;

import card.Card;
import card.CardGamePlayer;
import card.CardList;
import card.Hand;

public class FullHouse extends Hand {
    public FullHouse(CardGamePlayer player, CardList cards) {
        super(player, cards);
    }

    public String getType() {
        return "FullHouse";
    }

    public boolean isValid() {
        sort();
        if (this.size() == 5 && this.getCard(0).getRank() == this.getCard(1).getRank() && this.getCard(1).getRank() == this.getCard(2).getRank() && this.getCard(3).getRank() == this.getCard(4).getRank()) {

            return true;
        } else if (this.size() == 5 && this.getCard(0).getRank() == this.getCard(1).getRank() && this.getCard(2).getRank() == this.getCard(3).getRank() && this.getCard(3).getRank() == this.getCard(4).getRank()) {
            return true;
        } else {
            return false;
        }

    }

    public Card getTopCard() {
        sort();
        if (this.getCard(0).getRank() == this.getCard(1).getRank() && this.getCard(1).getRank() == this.getCard(2).getRank() && this.getCard(3).getRank() == this.getCard(4).getRank()) {
            return (this.getCard(0));
        } else {
            return this.getCard(4);
        }
    }


    public boolean beats(Hand hand) {
        if (hand.getType() == "Straight") {
            return true;
        } else if (hand.getType() == "Flush") {
            return true;
        } else if (hand.getType() == "FullHouse") {
            if (this.getTopCard().compareTo(hand.getTopCard()) == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }


    }
}
