package big2.hands;

import card.CardGamePlayer;
import card.CardList;
import card.Hand;

public class Flush extends Hand {
	public Flush(CardGamePlayer player, CardList cards) {
		super(player, cards);
	}
	public String getType() {
		return "Flush";
	}
	public boolean isValid() {
		sort();
		if (this.size() == 5 && this.getCard(0).getSuit() == this.getCard(1).getSuit() && this.getCard(1).getSuit() == this.getCard(2).getSuit() && this.getCard(2).getSuit() == this.getCard(3).getSuit() && this.getCard(3).getSuit() == this.getCard(4).getSuit()) {

			return true;
		}

		else {
			return false;
		}
	}
	public boolean beats(Hand hand) {
		if (hand.getType() == "Flush") {
			if (this.getCard(0).getSuit() == hand.getCard(0).getSuit()) {
				if (this.getTopCard().compareTo(hand.getTopCard()) == 1) {
					return true;
				}
				else {
					return false;
				}
			}
			else if (this.getCard(0).getSuit() > hand.getCard(0).getSuit()) {
				return true;
			}
			return false;
		}
		else if (hand.getType() == "Straight") {
			return true;
		}
		return false;
	}
}
