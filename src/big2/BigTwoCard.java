package big2;

import card.Card;

public class BigTwoCard extends Card {
    public BigTwoCard (int suit, int rank){
        super(suit,rank);
    }


    /**
     * Compares this card with the specified card for order.
     *
     * @param card
     *            the card to be compared
     * @return a negative integer, zero, or a positive integer as this card is
     *         less than, equal to, or greater than the specified card
     */
    public int compareTo(Card card) {
        int thisRank = this.rank;
        int thatRank = card.getRank();

        if (this.rank < 2) {
            thisRank += 13;
        }
        if (card.getRank() < 2) {
            thatRank += 13;
        }

        if (thisRank > thatRank) {
            return 1;
        } else if (thisRank < thatRank) {
            return -1;
        } else if (this.suit > card.getSuit()) {
            return 1;
        } else if (this.suit < card.getSuit()) {
            return -1;
        } else {
            return 0;
        }

    }

}


