package big2;

import card.CardGame;
import card.CardGameTable;
import card.CardList;
import card.Hand;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * big2.BigTwoTable encapsulate the game table.
 */
public class BigTwoTable implements CardGameTable {

    private CardGame game;
    private boolean[] selected;
    private int activePlayer;
    private int activeSelection;
    private JFrame frame;
    private JPanel bigTwoPanel;
    private JButton playButton;
    private JButton passButton;
    private JTextArea textArea;
    private Image[][] cardImages;
    private Image cardBackImage;
    private Image[] avatars;

    private class BigTwoPanel extends JPanel implements MouseListener {

        public BigTwoPanel() {
            this.setSize(300, 600);
            this.addMouseListener(this);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            int cardIdx = determineOnClickPosition(e.getX(), e.getY());
            if (cardIdx != -1) {
                selected[cardIdx] = ! selected[cardIdx];
            }
            BigTwoTable.this.repaint();
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    private class PlayButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            game.checkMove(getSelected());
        }
    }

    private class PassButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            game.checkMove(null);
        }
    }

    private class RestartMenuItemListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            game.start();
        }
    }

    private class QuitMenuItemListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    private JPanel initBigTwoPanel() {

        JPanel leftPanel = new JPanel();
        BoxLayout bigTwoLayout = new BoxLayout(leftPanel, BoxLayout.Y_AXIS);
        leftPanel.setLayout(bigTwoLayout);

        bigTwoPanel = new BigTwoPanel();
        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));

        playButton = new JButton("Play");
        passButton = new JButton("Pass");
        btnPanel.add(playButton);
        btnPanel.add(passButton);

        playButton.addActionListener(new PlayButtonListener());
        passButton.addActionListener(new PassButtonListener());

        leftPanel.add(bigTwoPanel);
        leftPanel.add(btnPanel);

        return leftPanel;
    }

    private void initPanel() {

        JPanel leftPanel = initBigTwoPanel();
        textArea = new JTextArea();

        //Create the menu bar.
        JMenuBar menuBar = new JMenuBar();
        JMenuItem menuGame = new JMenu("Game");
        JMenuItem btnRestart = new JMenuItem("Restart");
        JMenuItem btnQuit = new JMenuItem("Quit");

        btnRestart.addActionListener(new RestartMenuItemListener());
        btnQuit.addActionListener(new QuitMenuItemListener());

        menuGame.add(btnRestart);
        menuGame.add(btnQuit);
        menuBar.add(menuGame);

        frame = new JFrame("Big Two Panel");

        GridLayout frameLayout = new GridLayout(1, 2);

        frame.setLayout(frameLayout);

        frame.add(leftPanel);
        frame.add(textArea);
        frame.setJMenuBar(menuBar);

        frame.setSize(1000, 800);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

    private void loadImages() {
        cardImages = new Image[4][13];
        char[] suits = {'d', 'c', 'h', 's'};
        char[] ranks = {'a', '2', '3', '4', '5', '6', '7', '8', '9', 't', 'j', 'q', 'k'};

        try {

            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 13; j++) {
                    cardImages[i][j] = ImageIO.read(new File("img/card/" + ranks[j] + suits[i] + ".gif"));
                }
            }

            cardBackImage = ImageIO.read(new File("img/card/back.gif"));

            avatars = new Image[4];
            for (int i = 0; i < 4; i++) {
                avatars[i] = ImageIO.read(new File("img/avatar/p" + i + ".png"));
            }
        } catch (IOException ex) {
            throw new RuntimeException("Unable to load images!!", ex);
        }

    }

    /**
     * constructor of big2.BigTwoTable
     *
     * @param game
     */
    public BigTwoTable(CardGame game) {

        initPanel();
        loadImages();

        this.game = game;

        repaint();
    }

    /**
     * Sets the index of the active player (i.e., the current player).
     *
     * @param activePlayer an int value representing the index of the active player
     */
    public void setActivePlayer(int activePlayer) {
        this.activePlayer = activePlayer;
        this.resetSelected();
    }

    /**
     * Sets the index of the active selection (i.e., the index of the player
     * from whom the current player can pick the cards to make a move).
     *
     * @param activeSelection an int value representing the index of the active selection.
     */
    public void setActiveSelection(int activeSelection) {
        this.activeSelection = activeSelection;
    }

    /**
     * Returns an array of indices of the cards selected.
     *
     * @return an array of indices of the cards selected
     */
    public int[] getSelected() {
        ArrayList<Integer> selectedIndex = new ArrayList<Integer>();
        for (int i = 0; i < selected.length; i++) {
            if (selected[i]) {
                selectedIndex.add(i);
            }
        }

        int[] ret = new int[selectedIndex.size()];
        for (int i=0; i < ret.length; i++)
        {
            ret[i] = selectedIndex.get(i);
        }
        return ret;
    }

    /**
     * Resets the list of selected cards to an empty list.
     */
    public void resetSelected() {
        selected = new boolean[game.getPlayerList().get(activePlayer).getNumOfCards()];
    }

    /**
     * Given x and y position, determine if
     * @param x
     * @param y
     * @return
     */
    private int determineOnClickPosition(int x, int y) {

        int playerIdx = activeSelection;

        int numOfCards = game.getPlayerList().get(playerIdx).getNumOfCards();
        for (int i = numOfCards - 1; i >= 0; i--) {
            int cardX = 150 + i * 20;
            int cardY = playerIdx * 130 + 50;
            if (selected[i]) {
                cardY -= 20;
            }

            if (x > cardX && x < cardX + 73 && y > cardY && y < cardY + 97) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Repaints the GUI.
     */
    public void repaint() {

        Graphics graphics = bigTwoPanel.getGraphics();
        bigTwoPanel.paintComponents(graphics);
        graphics.clearRect(0, 0, bigTwoPanel.getWidth(), bigTwoPanel.getHeight());

        int currentPlayer = game.getCurrentIdx();

        for (int i = 0; i < game.getNumOfPlayers(); i++) {
            // render player
            // render the avatar

            graphics.drawImage(avatars[i], 10, 30 + i * 130, 100, 100, null);
            graphics.drawString(game.getPlayerList().get(i).getName(), 10, 10 + i * 130);

            if (currentPlayer == i) {

                // render with real card content
                int numOfCards = game.getPlayerList().get(i).getNumOfCards();
                CardList hands = game.getPlayerList().get(i).getCardsInHand();
                for (int j = 0; j < numOfCards; j++) {

                    int cardX = 150 + j * 20;
                    int cardY = i * 130 + 50;
                    if (selected[j]) {
                        cardY -= 20;
                    }

                    graphics.drawImage(cardImages[hands.getCard(j).getSuit()][hands.getCard(j).getRank()], cardX, cardY, 73, 97, null);
                }

            } else {

                // render cardback
                int numOfCards = game.getPlayerList().get(i).getNumOfCards();
                for (int j = 0; j < numOfCards; j++) {
                    graphics.drawImage(cardBackImage, 150 + j * 20, 50 + i * 130, 73, 97, null);
                }
            }
        }

        // render last hand
        ArrayList<Hand> hands = game.getHandsOnTable();
        if (hands.size() != 0) {
            Hand lastHand = hands.get(hands.size() - 1);

            graphics.drawString("Played by " + lastHand.getPlayer().getName(), 10, 580);

            for (int j = 0; j < lastHand.size(); j++) {

                int cardX = 70 + j * 20;
                int cardY = 590;

                graphics.drawImage(cardImages[lastHand.getCard(j).getSuit()][lastHand.getCard(j).getRank()], cardX, cardY, 73, 97, null);
            }
        }
    }

    /**
     * Prints the specified string to the text area of the card game table.
     *
     * @param msg the string to be printed to the text area of the card game
     *            table
     */
    public void print(String msg) {
        textArea.append(msg);
    }

    /**
     * Prints the specified string, followed by a newline, to the text area of
     * the card game table.
     *
     * @param msg the string to be printed to the text area of the card game
     *            table
     */
    public void println(String msg) {
        textArea.append(msg + "\n");
    }

    /**
     * Clears the text area of the card game table.
     */
    public void clearTextArea() {
        textArea.setText("");
    }

    /**
     * Resets the GUI.
     */
    public void reset() {

    }

    /**
     * Enables user interactions.
     */
    public void enable() {

    }

    /**
     * Disables user interactions.
     */
    public void disable() {

    }

}

