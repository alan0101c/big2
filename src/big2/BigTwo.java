package big2;

import big2.hands.*;
import big2.hands.Double;
import card.*;

import java.util.*;


public class BigTwo implements CardGame {

    private int numOfPlayers;
    private Deck deck;
    private ArrayList<CardGamePlayer> playerList;
    private ArrayList<Hand> handsOnTable;
    private int currentIdx;
    private boolean firstMove;
    private BigTwoTable table;


    public static void main(String[] args) {

        BigTwo mainGame = new BigTwo();
        mainGame.start();
    }

    public BigTwo() {

        playerList = new ArrayList<CardGamePlayer>();
        handsOnTable = new ArrayList<Hand>();

        // create 4 players
        numOfPlayers = 4;
        for (int i = 0; i < 4; i++) {
            playerList.add(new CardGamePlayer());
        }

        // initialize class variables
        table = new BigTwoTable(this);

    }

    /**
     * Returns the number of players in this card game.
     *
     * @return the number of players in this card game
     */
    public int getNumOfPlayers() {
        return numOfPlayers;
    }

    /**
     * Returns the deck of cards being used in this card game.
     *
     * @return the deck of cards being used in this card game
     */
    public Deck getDeck() {
        return deck;
    }

    /**
     * Returns the list of players in this card game.
     *
     * @return the list of players in this card game
     */
    public ArrayList<CardGamePlayer> getPlayerList() {
        return playerList;
    }

    /**
     * Returns the list of hands played on the table.
     *
     * @return the list of hands played on the table
     */
    public ArrayList<Hand> getHandsOnTable() {
        return handsOnTable;
    }

    /**
     * Returns the index of the current player.
     *
     * @return the index of the current player
     */
    public int getCurrentIdx() {
        return currentIdx;
    }

    /**
     * Identify the owner of a specific card, if no one owns the card, -1 is returned.
     *
     * @param card
     * @return
     */
    private int ownerOf(Card card) {

        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).getCardsInHand().contains(card))
                return i;
        }
        return -1;
    }


    /**
     * Validate if a hand of card is a valid move.
     *
     * @param player
     * @param cards
     * @return
     */
    private boolean isValidHand(CardGamePlayer player, CardList cards) {
        return getHand(player, cards) != null;
    }
    /**
     * Return the hand in case it is valid.
     * Null if otherwise.
     *
     * @param player
     * @param cards
     * @return
     */
    private Hand getHand(CardGamePlayer player, CardList cards) {

        Hand hand;

        if (cards != null)
        {
            hand = new Single(player, cards);//�����Ƿ�Ϊsingle
            if (hand.isValid())
                return hand;

            hand = new Double(player, cards);//�����Ƿ�Ϊdouble
            if (hand.isValid())
                return hand;

            hand = new Triple(player, cards);//�����Ƿ�Ϊtrip
            if (hand.isValid())
                return hand;

            hand = new StraightFlush(player, cards);//�����Ƿ�Ϊtrip
            if (hand.isValid())
                return hand;

            hand = new Straight(player, cards);//�����Ƿ�Ϊtrip
            if (hand.isValid())
                return hand;

            hand = new Quad(player, cards);//�����Ƿ�Ϊtrip
            if (hand.isValid())
                return hand;

            hand = new FullHouse(player, cards);//�����Ƿ�Ϊtrip
            if (hand.isValid())
                return hand;

            hand = new Flush(player, cards);//�����Ƿ�Ϊtrip
            if (hand.isValid())
                return hand;
        }

        return null;
    }

    private void playCard(CardGamePlayer player, Hand hand) {

        // put hand only if it is valid
        if (hand != null) {
            handsOnTable.add(hand);
            player.removeCards(hand);
            table.print("{" + hand.getType() + "}");
            table.println(hand.toString());
            firstMove = false;
        }

        // advance player
        currentIdx = (currentIdx + 1) % 4;
        printCurrentPlayer();
        table.setActivePlayer(currentIdx);
        table.setActiveSelection(currentIdx);
        table.resetSelected();
        table.repaint();

    }

    /**
     * Validate whether a list of card is valid for the first mover.
     * @param wantToShow
     * @return
     */
    private boolean validFirstMove(CardList wantToShow) {
        // 1. must be a valid move and must not pass.
        if (wantToShow == null || !isValidHand(playerList.get(currentIdx), wantToShow)) {
            table.println("Not a valid hand!");
            return false;
        }

        // 2. must contain card (0, 2)
        if (!wantToShow.contains(new Card(0, 2))) {
            table.println("Must contains Tiles 3.");
            return false;
        }

        return true;
    }
    /**
     * Validate whether a list of card is valid.
     * @param wantToShow
     * @return
     */
    private boolean validMove(CardList wantToShow) {

        // 1. if pass, must not be current owner of deck
        if (wantToShow == null && lastHand().getPlayer().equals(playerList.get(currentIdx))) {
            table.println("You are on the lead, pick any hand.");
            return false;
        }

        if (wantToShow == null) {
            table.println("Pass!");
            return true;
        }

        // 2. must be valid move
        if (!isValidHand(playerList.get(currentIdx), wantToShow)) {
            table.println("Not a valid hand!");
            return false;
        }


        Hand candidateHand = getHand(playerList.get(currentIdx), wantToShow);

        // 3. if not pass, must be > current deck
        if (wantToShow != null && !candidateHand.beats(lastHand()) && !lastHand().getPlayer().equals(playerList.get(currentIdx))) {
            table.println("Please pick a valid hand or pass.");
            return false;
        }

        // 4. if not pass && current owner, any valid deck will do.
        return true;
    }

    /**
     * (Re-)Start the card game.
     */
    public void start() {

        // reset the tables
        handsOnTable = new ArrayList<Hand>();
        deck = new BigTwoDeck();
        deck.initialize();
        deck.shuffle();

        for (int i = 0; i < 4; i++) {
            playerList.get(i).getCardsInHand().removeAllCards();
        }

        for (int i = 0; i < deck.size(); i++) {
            playerList.get(i % 4).addCard(deck.getCard(i));
        }
        for (int i = 0; i < 4; i++) {
            playerList.get(i).getCardsInHand().sort();
        }

        // find the first mover
        currentIdx = ownerOf(new Card(0, 2));
        firstMove = true;

        table.setActivePlayer(currentIdx);
        table.setActiveSelection(currentIdx);
        printCurrentPlayer();
        table.repaint();
    }

    private void printCurrentPlayer() {
        table.println(getPlayerList().get(currentIdx).getName() + "'s Turn:");
    }
    /**
     * Return the last played hand on the table.
     * Null if nothing has been played yet.
     */
    private Hand lastHand() {
        if (handsOnTable.size() != 0) {
            return handsOnTable.get(handsOnTable.size() - 1);
        }
        return null;
    }

    /**
     * Checks the move made by the current player.
     *
     * @param cardIdx the list of the indices of the cards selected by the current player
     */
    public void checkMove(int[] cardIdx) {
        CardList wantToShow = playerList.get(currentIdx).play(cardIdx);
        if (firstMove) {
            if (validFirstMove(wantToShow)) {
                playCard(playerList.get(currentIdx), getHand(playerList.get(currentIdx), wantToShow));
            }
        } else {
            if (validMove(wantToShow)) {
                playCard(playerList.get(currentIdx), getHand(playerList.get(currentIdx), wantToShow));
            }
        }
    }

    /**
     * Checks for end of game.
     *
     * @return true if the game ends; false otherwise
     */
    public boolean endOfGame() {
        for (int i = 0; i < 4; i++) {
            if (playerList.get(i).getNumOfCards() == 0){
                table.println("Game over! Winner is - " + lastHand().getPlayer().getName());
                return true;
            }
        }
        return false;
    }
}


