package card;

public abstract class Hand extends CardList {

    private CardGamePlayer player;
    private String type;
    private int typeId;

    public Hand(CardGamePlayer player, CardList cards) {
        this.player = player;
        for (int i = 0; i < cards.size(); i++)
            this.addCard(cards.getCard(i));
        this.sort();
    }

    public CardGamePlayer getPlayer() {
        return player;
    }

    public Card getTopCard() {
        return getCard(0);
    }

    public boolean beats(Hand hand) {
        if (this.getTopCard().compareTo(hand.getTopCard()) > 0)
            return true;
        else {

            return false;
        }
    }

    public abstract boolean isValid();

    public abstract String getType();

}
